from random import randint

name = input("Hi! What is your name?")


for guess_number in range(5):

    month = randint(1,12)
    year = randint(1924,2004)

    print ("Guess", guess_number + 1, ":", name, "were you born in", month, "/", year, "?")
    yes_or_no = input("yes or no?")

    if yes_or_no == "yes":
        print("I knew it")
        exit()
    elif guess_number == 4:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again")
